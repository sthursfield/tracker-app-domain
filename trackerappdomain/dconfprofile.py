# Tracker App Domain
# Copyright (C) 2018  Sam Thursfield <sam@afuera.me.uk>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this program; if not, see <http://www.gnu.org/licenses/>.


from gi.repository import GLib

import logging
import os
import subprocess

log = logging.getLogger("trackerappdomain")


class _DConfProfile:
    def __init__(self, bus, domain_name, datadir):
        # To configure our Tracker daemons separately from the session wide
        # instance of Tracker, we set up a separate DConf profile.

        # Data will be stored in $XDG_CONFIG_HOME/dconf/$database.
        # the filename that we can use here must be a valid D-Bus object
        # path, or DConf will throw an internal assertion.
        database_name = domain_name.replace('-', '_').replace('.', '_')

        # This is the profile description file
        os.makedirs(os.path.join(datadir, 'dconf'), exist_ok=True)
        self.path = os.path.join(datadir, 'dconf', 'profile')
        with open(self.path, 'w') as f:
            f.write('user-db:{}'.format(database_name))

        # We need to set DCONF_SESSION_BUS_ADDRESS when calling `gsettings` so
        # that change notifications happen on the private bus where our Tracker
        # daemons are listening for changes.
        self._bus = bus

    def set_value(self, schema, key, value):
        # We have to use a subprocess to change the settings so that we can be
        # sure that DCONF_PROFILE has an effect. GLib lacks any API that would
        # allow us to reliably create a DConfSettingsBackend that we could use
        # in this process that would write to the correct profile.
        log.debug("dconfprofile: Set {}.{} to {}".format(schema, key, value.print_(False)))
        env = os.environ.copy()
        env['DBUS_SESSION_BUS_ADDRESS'] = self._bus.address()
        env['DCONF_PROFILE'] = self.path
        subprocess.run(['gsettings', 'set', schema, key, value.print_(False)],
                       check=True, env=env)

    def get_value(self, schema, key):
        # We can't use GLib.Settings API here, see the comment in set_value().
        env = os.environ.copy()
        env['DBUS_SESSION_BUS_ADDRESS'] = self._bus.address()
        env['DCONF_PROFILE'] = self.path
        if 'G_MESSAGES_DEBUG' in env:
            del env['G_MESSAGES_DEBUG']
        result = subprocess.run(['gsettings', 'get', schema, key], check=True,
                                env=env, stdout=subprocess.PIPE)
        value = GLib.Variant.parse(GLib.VariantType('as'), result.stdout.decode('utf-8').strip())
        log.debug("dconfprofile: Got value {} for {}.{}".format(result.stdout.decode('utf-8'), schema, key))
        return value

    def reset_recursively(self, schema):
        '''Set all values in the given schema back to the default value.

        This is intended mainly for use by unit tests. Do not use this on real
        user settings unless you are sure that this is what the user wants!

        '''
        env = os.environ.copy()
        env['DCONF_PROFILE'] = self.path
        result = subprocess.run(['gsettings', 'reset-recursively', schema], check=True,
                                env=env, stdout=subprocess.PIPE)
        log.debug("dconfprofile: Reset all keys in {}".format(schema))
