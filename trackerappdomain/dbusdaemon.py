# Tracker App Domain
# Copyright (C) 2018  Sam Thursfield <sam@afuera.me.uk>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this program; if not, see <http://www.gnu.org/licenses/>.


from gi.repository import Gio, GLib

import logging
import os
import signal
import subprocess
import tempfile
import threading

log = logging.getLogger("trackerappdomain")


def pipe_to_log(pipe):
    while True:
        line = pipe.readline()
        if len(line) == 0:
            break
        log.debug("> {}".format(line.decode('utf-8').rstrip()))


class _DBusDaemon:
    def __init__(self, datadir):
        self._process = None
        self._address = None

        self._config_file = None

        self._datadir = datadir

        self._gdbus_connection = None

    def _configuration(self):
        return '\n'.join([
            '<busconfig>',
            '  <type>session</type>',
            '  <listen>unix:tmpdir=/tmp</listen>',
            '  <standard_session_servicedirs />',
            '  <policy context=\"default\">',
            '    <!-- Allow everything to be sent -->',
            '    <allow send_destination=\"*\" eavesdrop=\"true\"/>',
            '    <!-- Allow everything to be received -->',
            '    <allow eavesdrop=\"true\"/>',
            '    <!-- Allow anyone to own anything -->',
            '    <allow own=\"*\"/>',
            '  </policy>',
            '</busconfig>'])

    def start_async(self, callback, extra_env={}):
        fd, self._config_file = tempfile.mkstemp(prefix='tracker-app-domain-dbus-config-', dir=self._datadir)
        with os.fdopen(fd, 'w') as f:
            f.write(self._configuration())

        command = ['dbus-daemon', '--config-file={}'.format(self._config_file),
                   '--print-address=1', '--print-pid=1']

        log.debug("Starting DBus daemon: {}".format(command))
        env = os.environ.copy()
        env.update(extra_env)

        # We start the process in a separate session, otherwise interrupting
        # the main process causes the D-Bus daemon to shut down which is super
        # annoying when trying to debug the main process in a debugger as it
        # makes everything go haywire when execution is resumed.
        #
        # The downside to this is we have to manually ensure the daemon is
        # killed when the parent process is terminated.
        self._process = subprocess.Popen(command, stdout=subprocess.PIPE,
                                         stderr=subprocess.PIPE, env=env,
                                         start_new_session=True)

        self._previous_sigterm_handler = signal.signal(signal.SIGTERM, self._sigterm_handler)

        self._address = self._process.stdout.readline().strip().decode('ascii')
        try:
            pid = int(self._process.stdout.readline().strip().decode('ascii'))
        except ValueError as e:
            raise RuntimeError("Failed to start private D-Bus daemon.\n{}".format(self._process.stderr.read().strip().decode('unicode-escape')))
        log.debug("Got PID {}, address {}".format(pid, self._address))

        self._gdbus_connection = Gio.DBusConnection.new_for_address_sync(
            self._address,
            Gio.DBusConnectionFlags.AUTHENTICATION_CLIENT | Gio.DBusConnectionFlags.MESSAGE_BUS_CONNECTION,
            None, None)

        self._threads=[threading.Thread(target=pipe_to_log, args=(self._process.stdout,), daemon=True),
                       threading.Thread(target=pipe_to_log, args=(self._process.stderr,), daemon=True)]
        self._threads[0].start()
        self._threads[1].start()

        # Finish by sending a message to the daemon and waiting for a response.
        # This ensures that we get an exception right away if something has
        # gone wrong.
        def ping_callback(conn, res):
            logging.debug("Ping call returned.")
            conn.call_finish(res)
            GLib.idle_add(callback)

        logging.debug("Pinging the new D-Bus daemon...")
        self._gdbus_connection.call(
            'org.freedesktop.DBus', '/', 'org.freedesktop.DBus', 'GetId',
            None, None, Gio.DBusCallFlags.NONE, 10000, None, ping_callback)

    def _sigterm_handler(self, signal, frame):
        self.stop()

    def stop(self):
        log.debug("Stopping DBus daemon")
        self._process.terminate()
        self._process.wait()
        if os.path.exists(self._config_file):
            os.remove(self._config_file)
        signal.signal(signal.SIGTERM, self._previous_sigterm_handler)

    def connection(self):
        return self._gdbus_connection

    def address(self):
        return self._address
