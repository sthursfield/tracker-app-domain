# Tracker App Domain
# Copyright (C) 2018  Sam Thursfield <sam@afuera.me.uk>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this program; if not, see <http://www.gnu.org/licenses/>.

'''Tracker App Domain

The idea of this mini library is to allow embedding the Tracker database and
indexer tools into arbitrary applications.

The new "domains" feature of Tracker 2.0 allows this for certain use cases, but
with the following limits:

 * you need to install domain rules into XDG_DATA_DIR (usually
   /usr/share/tracker) before you can use them with set_domain(), so domains
   used this way can only be created by the system administrator.

 * you need to install miner service files into XDG_DATA_DIR (again,
   /usr/share/tracker) for TrackerMinerManager to be aware of miners, which
   again means domains must be created by the system administrator.

 * the store and miners need to have D-Bus service files installed that
   correspond to the name of the domain so that D-Bus autoactivation can be
   used to start them. This again requires write access to XDG_DATA_DIR.

By starting the daemons manually on a separate bus, this function does not
have those limits. It also provides some convience API for using the domain.

Eventually this library should go away and Tracker's API should be changed to
support all the features implemented here. Consider this library a "proving
ground" to try and work out what the final API should look like.

Each Tracker "domain" is an isolated instance of the Tracker data store and
miner daemons. No information is shared between different domains and you
cannot do aggregate queries across them.

This function lets you set up a domain from your application. It will run on
an isolated D-Bus daemon and all data will be stored in 'datadir'.

Note that you can only use ONE Tracker domain in a process. This is because of
the global nature of tracker_sparql_connection_set_domain().

'''

import gi
gi.require_version('Tracker', '2.0')
from gi.repository import Gio, GLib, GObject, Tracker

import configparser
import contextlib
import logging
import math
import os
import subprocess
import sys
import urllib.request

from . import dbusdaemon
from . import dconfprofile


log = logging.getLogger("trackerappdomain")

G_SOURCE_REMOVE = False
G_SOURCE_CONTINUE = True


# Rate limit in milliseconds for the tracker-store's GraphUpdated signal.
# This defaults to 1000 for the session-wide instance to minimize background
# performance problems. Since we expect to be a user-driven process we can
# be more aggressive and reduce delays in components that rely on these change
# notification signals from the store.
GRAPHUPDATED_DELAY = 100


class TrackerAppDomainError(RuntimeError):
    pass


class MainLoop():
    '''Wrapper for GLib.MainLoop that propagates any unhandled exceptions.

    PyGObject doesn't seem to provide any help with propagating exceptions from
    the GLib main loop to the main Python execution context. The default
    behaviour is to print a message and continue, which is useless for tests as
    it means tests appear to pass when in fact they are broken.

    This class allows any such exceptions to be reported back to Pytest.

    '''

    def __init__(self):
        self._loop = GLib.MainLoop.new(None, 0)

    def quit(self):
        self._loop.quit()

    def run_checked(self):
        '''Run the loop until quit(), then raise any unhandled exception.'''
        self._exception = None

        old_hook = sys.excepthook

        def new_hook(etype, evalue, etb):
            log.debug("Uncaught exception {}; exiting GLib main loop".format(etype))
            self._loop.quit()
            self._exception = evalue
            old_hook(etype, evalue, etb)

        try:
            sys.excepthook = new_hook
            self._loop.run()
        finally:
            sys.excepthook = old_hook

        if self._exception:
            raise self._exception   # Re-raise exception from GLib main loop.


class _TrackerDaemon():
    def __init__(self, name, dbus_name, dbus_object_path):
        self.name = name
        self.dbus_name = dbus_name
        self.dbus_object_path = dbus_object_path

    def __str__(self):
        return self.name

    def __repr__(self):
        return "<{}: {}>".format(self.__class__.__name__, self.name)

    def dbus_proxy(self, dbus_interface_name):
        '''Return a GDBusProxy for this daemon, for a given DBus interface.'''
        return Gio.DBusProxy.new_for_bus_sync(
            Gio.BusType.SESSION,
            Gio.DBusProxyFlags.DO_NOT_AUTO_START_AT_CONSTRUCTION,
            None,
            self.dbus_name,
            self.dbus_object_path,
            dbus_interface_name,
            None)

    # This only works for daemons that implement org.freedesktop.Tracker1.Miner
    #
    # Note that we cannot use the libtracker-control TrackerMinerManager API to
    # call this D-Bus interface as that only works for miners installed into
    # /usr.
    def miner_iface(self):
        if not self._miner_iface:
            self._miner_iface = self.dbus_proxy('org.freedesktop.Tracker1.Miner')
        return self._miner_iface


class _TrackerAppDomainDaemon(_TrackerDaemon):
    def __init__(self, bus, dconf_profile, tracker_domain_rule, dbus_name,
                 dbus_object_path, executable_path):
        name = os.path.basename(executable_path)

        super(_TrackerAppDomainDaemon, self).__init__(name, dbus_name, dbus_object_path)

        self.tracker_domain_rule = tracker_domain_rule

        self.dconf_profile = dconf_profile

        self._bus = bus
        self._executable_path = executable_path
        self._on_bus = False

        self._miner_iface = None

    def write_dbus_service_file(self, share_dir, extra_args=[]):
        config = configparser.ConfigParser()
        config.optionxform=str  # We must avoid lowercasing the section names
        config.add_section('D-BUS Service')
        config.set('D-BUS Service', 'Name', self.dbus_name)

        extra_env = [
            'DCONF_PROFILE={}'.format(self.dconf_profile.path),
        ]

        commandline = ['/usr/bin/env'] + extra_env + [self._executable_path,
                       '--domain-ontology', self.tracker_domain_rule] + extra_args
        config.set('D-BUS Service', 'Exec',  ' '.join(commandline))

        path = os.path.join(share_dir, 'dbus-1', 'services', self.dbus_name + '.service')
        os.makedirs(os.path.dirname(path), exist_ok=True)
        with open(path, 'w') as f:
            config.write(f)
        return path

    def dbus_proxy(self, dbus_interface_name):
        return Gio.DBusProxy.new_sync(self._bus.connection(),
                                      Gio.DBusProxyFlags.DO_NOT_AUTO_START_AT_CONSTRUCTION,
                                      None,
                                      self.dbus_name,
                                      self.dbus_object_path,
                                      dbus_interface_name,
                                      None)

# These are the placeholders used by Tracker in its configuration to represent
# the XDG user directories.
XDG_DIRS = {
    '&DESKTOP': GLib.UserDirectory.DIRECTORY_DESKTOP,
    '&DOCUMENTS': GLib.UserDirectory.DIRECTORY_DOCUMENTS,
    '&DOWNLOAD': GLib.UserDirectory.DIRECTORY_DOWNLOAD,
    '&MUSIC': GLib.UserDirectory.DIRECTORY_MUSIC,
    '&PICTURES': GLib.UserDirectory.DIRECTORY_PICTURES,
    '&PUBLIC_SHARE': GLib.UserDirectory.DIRECTORY_PUBLIC_SHARE,
    '&TEMPLATES': GLib.UserDirectory.DIRECTORY_TEMPLATES,
    '&VIDEOS': GLib.UserDirectory.DIRECTORY_VIDEOS,
}

def expand_xdg_placeholders(path):
    '''Expand special placeholders found in Tracker directory configuration.

    The XDG standard content directories can be represented as e.g. &VIDEOS
    and &MUSIC. Note that expanding these can sometimes be returned unexpanded
    if the configured location doesn't actually exist.

    Environment variables can also be substituted using the normal shell syntax of
    $VAR or ${VAR}.

    '''
    if path in XDG_DIRS:
        expanded_path = GLib.get_user_special_dir(XDG_DIRS[path])
        if expanded_path is None:
            # It's pretty useless returning something like &MUSIC if the path
            # doesn't exist, but that's what Tracker does and it's at least
            # better than returning None.
            return path
        else:
            return expanded_path
    elif path.startswith('~'):
        return os.path.expanduser(path)

    # Tracker really supports embedding arbitrary environment variable
    # expansions into your configuration. I don't know if anyone uses this
    # other than for $HOME in the default value of index-single-directories,
    # but if a feature is there probably somebody relies on it ...
    def expand_env_var(part):
        if part.startswith('${'):
            return os.getenv(part[2:-1])
        elif part.startswith('$'):
            return os.getenv(part[1:])
        else:
            return part
    path = os.path.join(*[expand_env_var(part) for part in path.split(os.pathsep)])

    return path



# TrackerSessionDomain:
#
# Represents a session-wide Tracker domain. This simply provides some utility
# functions that extend Tracker's own API. It exists mainly so that users of
# TrackerAppDomain can also support using the session-wide Tracker instance
# without having to use two different code paths.
#
# If a non-default domain_name is specified, there must have a corresponding
# domain rule installed in the well-known location for domain rules, and the
# correct D-Bus .service files must also be created. You can avoid doing this
# by using the TrackerAppDomain subclass.
#
class TrackerSessionDomain(GObject.Object):
    def __init__(self, domain_name=None):
        super(TrackerSessionDomain, self).__init__()

        self._domain_name = domain_name

        self.started = False
        self._cancellable = Gio.Cancellable.new()

        self._miner_status = {}
        self._miner_progress = {}
        self._miner_remaining_time = {}

        self._miner_settings = Gio.Settings.new('org.freedesktop.Tracker.Miner.Files')

    def domain_name(self):
        return self._domain_name or 'org.freedesktop'

    def _setup_config_watches(self):
        self._miner_settings.connect('changed::index-recursive-directories',
                                     lambda settings, key: self._update_indexing_locations())
        self._miner_settings.connect('changed::index-single-directories',
                                     lambda settings, key: self._update_indexing_locations())
        self._update_indexing_locations()

    def _setup_miners(self):
        domain_name = self.domain_name()
        self._tracker_miner_fs = _TrackerDaemon(
            'tracker-miner-fs', domain_name + '.Tracker1.Miner.Files',
            '/org/freedesktop/Tracker1/Miner/Files')
        self._tracker_extract = _TrackerDaemon(
            'tracker-extract', domain_name + '.Tracker1.Miner.Extract',
            '/org/freedesktop/Tracker1/Miner/Extract')

        self._miners = [self._tracker_miner_fs, self._tracker_extract]

    def _setup_status_watches(self):
        for m in self._miners:
            proxy = m.dbus_proxy('org.freedesktop.Tracker1.Miner')
            proxy.connect('g-signal', self._on_miner_signal)

            self._miner_status[m] = 'Idle'
            self._miner_progress[m] = 'Idle'
            self._miner_remaining_time[m] = '0:00'

            def update_status(proxy, res, miner):
                if not self._cancellable.is_cancelled():
                    status = proxy.call_finish(res)
                    self._miner_status[miner] = status[0]
            proxy.call('GetStatus', None, Gio.DBusCallFlags.NONE, -1, self._cancellable, update_status, user_data=m)

            def update_progress(proxy, res, miner):
                if not self._cancellable.is_cancelled():
                    progress = proxy.call_finish(res)
                    self._miner_progress[miner] = progress[0]
            proxy.call('GetProgress', None, Gio.DBusCallFlags.NONE, -1, self._cancellable, update_progress, user_data=m)

            def update_remaining_time(proxy, res, miner):
                if not self._cancellable.is_cancelled():
                    remaining_time = proxy.call_finish(res)
                    self._miner_remaining_time[miner] = remaining_time[0]
            proxy.call('GetRemainingTime', None, Gio.DBusCallFlags.NONE, -1, self._cancellable, update_remaining_time, user_data=m)

            # Save the proxy on the object so that we can use it later to match
            # in the signal callback.
            m.__proxy = proxy

        self.notify('indexing')
        self.notify('status-message')

    # start_async():
    #
    # Set up the domain. This must be done before calling any other methods.
    def start_async(self, callback):
        self._setup_config_watches()
        self._setup_miners()
        self._setup_status_watches()

        self.started = True

        GLib.idle_add(callback)

    def stop(self):
        self._cancellable.cancel()
        self.started = False

    # connection():
    #
    # Returns a Tracker.SparqlConnection instance for talking to the Tracker
    # domain store.
    def connection(self):
        assert self.started

        if self._domain_name is not None:
            Tracker.SparqlConnection.set_domain(self._domain_name)

        return Tracker.SparqlConnection.get()

    # connection_async():
    #
    # Returns a Tracker.SparqlConnection instance for talking to the Tracker
    # domain store.
    def connection_async(self, callback):
        assert self.started

        if self._domain_name is not None:
            Tracker.SparqlConnection.set_domain(self._domain_name)

        return Tracker.SparqlConnection.get_async(None, callback)

    # add_indexing_location():
    #
    # Add directory to the list of locations configured for indexing.
    #
    # The Tracker miners will add any new content from this location to the
    # tracker-store as soon as possible.
    def add_indexing_location(self, path, recursive=True):
        key = 'index-recursive-directories' if recursive else 'index-single-directories'
        locations = self._miner_settings.get_value(key)
        self._miner_settings.set_value(key, GLib.Variant('as', list(locations) + [path]))
        self._update_indexing_locations()

    # remove_indexing_location():
    #
    # Remove directory from the list of locations configured for indexing.
    #
    # The Tracker filesystem miner will cancel any processing taking place in
    # the removed location.
    #
    # FIXME: does anything get removed frm the store though ???
    def remove_indexing_location(self, path):
        recursive_locations = list(self._miner_settings.get_value('index-recursive-directories'))
        single_locations = list(self._miner_settings.get_value('index-single-directories'))

        for i, expanded_path in enumerate(expand_xdg_placeholders(p) for p in recursive_locations):
            if expanded_path == path:
                del recursive_locations[i]
                self._miner_settings.set_value('index-recursive-directories',
                                               GLib.Variant('as', recursive_locations))
                break

        for i, expanded_path in enumerate(expand_xdg_placeholders(p) for p in single_locations):
            if expanded_path == path:
                del single_locations[i]
                self._miner_settings.set_value('index-single-directories',
                                               GLib.Variant('as', single_locations))
                break

        self._update_indexing_locations()

    # configured_indexing_locations:
    #
    # The list of locations that Tracker is configured to index,
    # as a list of tuples of (path, is_recursive).
    #
    # This reflects the user config from DConf/GSettings, it does not include
    # any extra locations that have been added by applications using the
    # index_location() method or the IndexFileForProcess D-Bus method.
    #
    # For the sesson-wide domain, this property will be updated whenever the
    # user configuration is changed. For private app domains, it is set once
    # on load and not updated.
    @GObject.Property(type=GLib.Array)
    def configured_indexing_locations(self):
        return self._configured_indexing_locations

    def _update_indexing_locations(self):
        locations = []
        locations.extend((expand_xdg_placeholders(path), True)
                      for path in self._miner_settings.get_value('index-recursive-directories'))
        locations.extend((expand_xdg_placeholders(path), False)
                      for path in self._miner_settings.get_value('index-single-directories'))
        self._configured_indexing_locations = locations
        self.notify('configured-indexing-locations')

    # indexing:
    #
    # True if any Tracker daemons are currently processing data, False if they
    # are all idle.
    @GObject.Property(type=bool, default=False)
    def indexing(self):
        assert self.started
        logging.debug("Statuses: {}".format(self._miner_status))
        if all(status == 'Idle' for status in self._miner_status.values()):
            return False
        else:
            return True

    # status_message:
    #
    # A textual status message that can be shown to users. Messages from the
    # filesystem miner show %-age completion. If the filesystem miner is idle,
    # messages from the extractor are shown which show which file is currently
    # being processed.
    @GObject.Property(type=str)
    def status_message(self):
        assert self.started
        if self._miner_status[self._tracker_miner_fs] == 'Idle':
            if self._miner_status[self._tracker_extract] == 'Idle':
                return 'Idle'
            else:
                status = '{} ({}% complete)'.format(
                    self._miner_status[self._tracker_extract],
                    math.ceil(self._miner_progress[self._tracker_extract] * 100))
                return status
        else:
            return self._miner_status[self._tracker_miner_fs]

    def _on_miner_signal(self, proxy, sender, signal, params):
        if signal == 'Progress':
            if proxy == self._tracker_miner_fs.__proxy:
                miner = self._tracker_miner_fs
            elif proxy == self._tracker_extract.__proxy:
                miner = self._tracker_extract
            else:
                logging.warn("Received signal from unknown miner proxy: {}".format(proxy))
                return

            logging.debug("Got {} from proxy {} ({})".format(params, proxy, miner))

            self._miner_status[miner] = params[0]
            self._miner_progress[miner] = params[1]
            self._miner_remaining_time[miner] = params[2]

            self.notify('indexing')
            self.notify('status-message')


# TrackerAppDomain:
#
# The class representing a Tracker domain that is private to the current
# process.
#
# You need to call start_async() before the domain can be used.
#
class TrackerAppDomain(TrackerSessionDomain):
    def __init__(self, domain_name, datadir, tracker_libexec_dir=None, debug_verbosity=0):
        assert domain_name != 'org.freedesktop', \
            "You must not use the default Tracker domain name of " \
            "org.freedesktop for app-specific domains."

        super(TrackerAppDomain, self).__init__(domain_name)

        self.datadir = os.path.abspath(datadir)
        self.debug_verbosity = debug_verbosity

        self._name_owner_id = None

        self._cache_home = os.path.join(self.datadir, 'tracker-cache')
        self._data_home = os.path.join(self.datadir, 'tracker-data')

        self._share_dir = self.datadir

        self.domain_rule = self._write_domain_rule()

        tracker_libexec_dir = tracker_libexec_dir or os.environ.get('TRACKER_APP_DOMAIN_LIBEXEC_DIR', '/usr/libexec')

        self._bus = dbusdaemon._DBusDaemon(self.datadir)
        self._dconf_profile = dconfprofile._DConfProfile(self._bus, domain_name, self.datadir)
        self._tracker_store = _TrackerAppDomainDaemon(
            self._bus, self._dconf_profile, self.domain_rule,
            domain_name + '.Tracker1', '/org/freedesktop/Tracker',
            os.environ.get('TRACKER_APP_DOMAIN_TRACKER_STORE_PATH',
                           os.path.join(tracker_libexec_dir, 'tracker-store')))

        self._setup_miners(tracker_libexec_dir)

        self._configured_indexing_locations = None

    def _setup_config_watches(self):
        # We can't watch config changes in an app domain as we can't access the
        # DConf profile using the GSettings API in-process. Instead we load the
        # config once from the start() method.
        #
        # FIXME: it is actually possible to get monitoring by running a
        # `gsettings monitor` subprocess; maybe we should do that.
        pass

    def _setup_miners(self, tracker_libexec_dir):
        self._tracker_miner_fs = _TrackerAppDomainDaemon(
            self._bus, self._dconf_profile, self.domain_rule,
            self._domain_name + '.Tracker1.Miner.Files',
            '/org/freedesktop/Tracker1/Miner/Files',
            os.environ.get('TRACKER_APP_DOMAIN_TRACKER_MINER_FS_PATH',
                            os.path.join(tracker_libexec_dir, 'tracker-miner-fs')))
        self._tracker_extract = _TrackerAppDomainDaemon(
            self._bus, self._dconf_profile, self.domain_rule,
            self._domain_name + '.Tracker1.Miner.Extract',
            '/org/freedesktop/Tracker1/Miner/Extract',
            os.environ.get('TRACKER_APP_DOMAIN_TRACKER_EXTRACT_PATH',
                            os.path.join(tracker_libexec_dir, 'tracker-extract')))

        self._miners = [self._tracker_miner_fs, self._tracker_extract]

        self._configured_indexing_locations = None

    def _setup_config_watches(self):
        # We can't watch config changes in an app domain as we can't access the
        # DConf profile using the GSettings API in-process. Instead we load the
        # config once from the start() method.
        pass

    def _write_domain_rule(self):
        config = configparser.ConfigParser()
        config.optionxform=str  # We must avoid lowercasing the section names
        config.add_section('DomainOntology')
        config.set('DomainOntology', 'CacheLocation', 'file://' + urllib.request.pathname2url(self._cache_home))
        config.set('DomainOntology', 'JournalLocation', 'file://' + urllib.request.pathname2url(self._data_home))
        config.set('DomainOntology', 'OntologyName', 'nepomuk')
        # This setting affects only the way that the miners and store are
        # activated. Since we know we are on an isolated message bus there's
        # no harm in using the default name, and this saves us from having to
        # create a bunch of D-Bus .service files using the actual app domain
        # name.
        config.set('DomainOntology', 'Domain', self._domain_name)

        rule_path = os.path.join(self._share_dir, 'tracker', 'domain-ontologies', self._domain_name + '.rule')
        os.makedirs(os.path.dirname(rule_path), exist_ok=True)
        with open(rule_path, 'w') as f:
            config.write(f)
        return rule_path

    # start_async():
    #
    # Sets up the domain, which involves starting the private D-Bus daemon.
    # The Tracker subprocesses are not started by this function, they will be
    # started as needed using D-Bus auto-activation.
    #
    def start_async(self, callback):
        os.makedirs(self._cache_home, exist_ok=True)
        os.makedirs(self._data_home, exist_ok=True)
        os.makedirs(self._share_dir, exist_ok=True)

        self._tracker_store.write_dbus_service_file(
            self._share_dir, extra_args=['--verbosity', str(self.debug_verbosity)])
        self._tracker_miner_fs.write_dbus_service_file(
            self._share_dir, extra_args=['--verbosity', str(self.debug_verbosity), '--initial-sleep', '0'])
        self._tracker_extract.write_dbus_service_file(
            self. _share_dir, extra_args=['--verbosity', str(self.debug_verbosity)])

        def dbus_ready():
            self._dconf_profile.set_value('org.freedesktop.Tracker.Store', 'graphupdated-delay', GLib.Variant.new_int32(GRAPHUPDATED_DELAY))

            # Tracker mainline doesn't yet support passing a full path to a domain
            # rule. See https://bugzilla.gnome.org/show_bug.cgi?id=795421.
            Tracker.SparqlConnection.set_domain(os.path.join(self._share_dir, 'tracker', 'domain-ontologies', self._domain_name + '.rule'))
            # This function is not yet in Tracker mainline. Patch to add it is
            # proposed in https://bugzilla.gnome.org/show_bug.cgi?id=795409
            Tracker.SparqlConnection.set_dbus_connection(self._bus.connection())

            self._update_indexing_locations()

            def name_acquired(connection, name):
                logging.debug("Acquired name {} on private message bus".format(name))
                self.started = True
                self._setup_status_watches()
                GLib.idle_add(callback)

            def name_lost(connection, name):
                raise TrackerAppDomainError("Lost name {} on bus".format(name))

            # We need to own the domain name on the bus, otherwise the
            # tracker-store will consider the domain inactive and will silently
            # shut down as soon as it is started.
            #
            # Something else may own the name on the public session bus, such as
            # a GtkApplication instance, but remember that app domains run a
            # separate, private message bus.
            self._name_owner_id = Gio.bus_own_name_on_connection(
                self._bus.connection(), self._domain_name,
                Gio.BusNameOwnerFlags.NONE, name_acquired, name_lost)


        xdg_data_dirs = os.environ.get('XDG_DATA_DIRS', '').split(':')
        extra_env = {'XDG_DATA_DIRS': ':'.join([self._share_dir] + xdg_data_dirs)}
        self._bus.start_async(callback=dbus_ready, extra_env=extra_env)

    # stop():
    #
    # Terminate the D-Bus daemon and all Tracker subprocesses started for this
    # domain.
    #
    def stop(self):
        self.started = False

        self._cancellable.cancel()

        if self._name_owner_id:
            Gio.bus_unown_name(self._name_owner_id)
            self._name_owner_id = None

        # Stopping must be done synchronously; if we run the GLib main loop after
        # the D-Bus daemon terminates then it exits the program altogether without
        # letting us finish. This is especially bad if we were trying to report an
        # unhandled exception to the user.
        self._bus.stop()
        log.debug("stop complete")

    def connection(self):
        assert self.started

        log.debug("Returning a TrackerSparqlConnection")
        return Tracker.SparqlConnection.get()

    def connection_async(self, callback):
        assert self.started

        log.debug("Returning a TrackerSparqlConnection asynchronously")
        return Tracker.SparqlConnection.get_async(None, callback)

    def _update_indexing_locations(self):
        index_recursive_directories = self._dconf_profile.get_value(
            'org.freedesktop.Tracker.Miner.Files', 'index-recursive-directories')
        index_single_directories = self._dconf_profile.get_value(
            'org.freedesktop.Tracker.Miner.Files', 'index-single-directories')

        locations = []
        locations.extend((expand_xdg_placeholders(path), True)
                    for path in index_recursive_directories)
        locations.extend((expand_xdg_placeholders(path), False)
                    for path in index_single_directories)
        self._configured_indexing_locations = locations
        self.notify('configured-indexing-locations')

    def add_indexing_location(self, path, recursive=True):
        key = 'index-recursive-directories' if recursive else 'index-single-directories'
        locations = self._dconf_profile.get_value('org.freedesktop.Tracker.Miner.Files', key)
        self._dconf_profile.set_value('org.freedesktop.Tracker.Miner.Files', key, GLib.Variant('as', list(locations) + [path]))
        self._update_indexing_locations()

    def remove_indexing_location(self, path):
        recursive_locations = list(self._dconf_profile.get_value('org.freedesktop.Tracker.Miner.Files', 'index-recursive-directories'))
        single_locations = list(self._dconf_profile.get_value('org.freedesktop.Tracker.Miner.Files', 'index-single-directories'))

        if path in recursive_locations:
            recursive_locations.remove(path)
            self._dconf_profile.set_value('org.freedesktop.Tracker.Miner.Files',
                                          'index-recursive-directories',
                                          GLib.Variant('as', recursive_locations))

        if path in single_locations:
            single_locations.remove(path)
            self._dconf_profile.set_value('org.freedesktop.Tracker.Miner.Files',
                                          'index-single-directories',
                                          GLib.Variant('as', single_locations))

        self._update_indexing_locations()

    # clear_indexing_locations():
    #
    # Remove all configured indexing locations.
    def clear_indexing_locations(self):
        self._dconf_profile.set_value('org.freedesktop.Tracker.Miner.Files', 'index-recursive-directories', GLib.Variant('as', []))
        self._dconf_profile.set_value('org.freedesktop.Tracker.Miner.Files', 'index-single-directories', GLib.Variant('as', []))
        self._update_indexing_locations()

    def reset_config(self):
        self._dconf_profile.reset_recursively('org.freedesktop.Tracker.Miner.Files')
        self._update_indexing_locations()

    # index_location_async():
    #
    # Recursively crawls the files within 'path' and stores information about
    # them into the Tracker domain store. Monitoring is set up for 'path' so
    # that any changes that take place on disk after the initial indexing will
    # be detected and reported by TrackerNotifier instances. The monitoring
    # will stop once all processes that requested for 'path' to be indexed
    # have stopped.
    #
    # The function returns immediately, and will continue through callbacks
    # from the GLib main loop. Crawling can take a long time, and the
    # information returned to 'progress_callback' can be useful for displaying
    # progress information.
    #
    # When the miner becomes idle, the idle_callback is called. The indexing
    # is guaranteed to have completed when this is called. Note that if more
    # indexing jobs are queued while this one is running, the idle_callback
    # will most likely not be called until all those jobs are also complete.
    #
    # If the location is deleted or unmounted before indexing completes,
    # idle_callback will also be called.
    #
    # Args:
    #   path (str): directory to recursively index
    #   progress_callback (callable): Called with progress info
    #   idle_callback (callable): Called when the miners have returned to 'idle' state
    #
    # FIXME: we could move this to the TrackerSessionDomain class, the issue is
    # that if other processes are using the miners too the callbacks may be
    # meaningless.
    def index_location_async(self, path, progress_callback, idle_callback):
        assert self.started

        task = _IndexLocationTask(self, path, progress_callback, idle_callback)
        task.start()


class _IndexLocationTask():
    '''This class implements the index_location_async() function.'''

    def __init__(self, domain, path, progress_callback, idle_callback):
        assert idle_callback
        progress_callback = progress_callback or (lambda *args: None)

        path = str(path)  # Don't break when we get passed a py.LocalPath from py.test
        if not os.path.exists(path):
            # Tracker does check for this case, but the error is returned to us
            # as a super weird thing that looks like this:
            #
            #   GDBus.Error:org.gtk.GDBus.UnmappedGError.Quark.__2dg_2dtype_2dprivate_2d_2dGTypeFlags.Code0
            #
            # Easier to just check outselves and raise an exception early.
            raise FileNotFoundError(path)

        self.domain = domain
        self.path = path
        self.progress_callback = progress_callback
        self.idle_callback = idle_callback

        self._started_miners = set()
        self._finished_miners = set()
        self._signal_connections = set()

        # We monitor the location being indexed in case it gets removed or
        # unmounted. This ensures that in these cases the task still calls
        # idle_callback, even if the Tracker daemons go a bit haywire and stop
        # sending status messages.
        self._monitor = Gio.File.new_for_path(path).monitor_directory(Gio.FileMonitorFlags.NONE, None)
        self._location_watch = self._monitor.connect('changed', self._location_changed_callback)

        self._idle_timeout = None

    def start(self):
        GLib.timeout_add(100, self._wait_for_idle)

    # We ensure all the miners are idle before enqueuing anything, so we
    # avoid getting confused by status messages from process startup.
    def _wait_for_idle(self):
        if all(status == 'Idle' for miner, status in self.domain._miner_status.items()):
            log.debug("Miners all idle, enqueuing file")
            self._enqueue_location()
            return G_SOURCE_REMOVE
        else:
            log.debug("Waiting for {} before starting ({} miners total)".format(
                ','.join(["{} (status: {})".format(miner, status)
                            for miner, status in self.domain._miner_status.items()
                            if status != 'Idle']), len(self.domain._miner_status)))
            return G_SOURCE_CONTINUE

    def _enqueue_location(self):
        for miner in self.domain._miners:
            connection = miner.miner_iface().connect('g-signal', self._miner_progress_callback, miner)
            self._signal_connections.add((miner, connection))

        url = 'file://' + urllib.request.pathname2url(os.path.abspath(self.path))
        log.debug("Calling IndexFileForProcess({})".format(url))

        self.domain._bus.connection().call(
            self.domain._domain_name + '.Tracker1.Miner.Files',
            '/org/freedesktop/Tracker1/Miner/Files/Index',
            'org.freedesktop.Tracker1.Miner.Files.Index',
            'IndexFileForProcess',
            GLib.Variant('(s)', (url,)), None,
            Gio.DBusCallFlags.NONE, 3000, None, self._start_callback)

    # This is called once IndexFileForProcess returns.
    def _start_callback(self, obj, res):
        log.debug("IndexFileForProcess call returned")
        try:
            obj.call_finish(res)
        except Exception as e:
            log.error("index_location() failed: {}".format(e))
            GLib.idle_add(self.idle_callback)

    # This is called each time one of the miners emits a status message, and
    # is used to drive the rest of the state changes in this object.
    def _miner_progress_callback(self, proxy, sender_name, signal_name, parameters, miner):
        if signal_name == 'Progress':
            status, progress, remaining_time = parameters
            log.debug("{}: Status: {}".format(miner, parameters))
            self.progress_callback(str(miner) + ': ' + status, progress, remaining_time)

            if status == 'Idle':
                if miner in self._started_miners:
                    log.debug("{}: finished".format(miner))
                    self._finished_miners.add(miner)

                    # We don't wait for all miners to go
                    # Idle->Processing->Idle because TrackerDecorator
                    # subclasses like tracker-extract won't ever leave the
                    # Idle state if none of the mtimes have changed. We do
                    # know that tracker-miner-fs always leaves Idle state,
                    # but we only wait for others if we saw them start
                    # processing.
                    if self.domain._tracker_miner_fs in self._finished_miners and len(self._finished_miners) == len(self._started_miners):
                        log.debug("All miners back to idle. Sleeping for two GraphUpdated delay cycles of "
                                    "{}ms".format(GRAPHUPDATED_DELAY*2))
                        self._idle_timeout = GLib.timeout_add(GRAPHUPDATED_DELAY*2, self._complete)
                    else:
                        log.debug("Still waiting for {}".format(self._started_miners.difference(self._finished_miners)))
            else:
                if miner not in self._started_miners:
                    log.debug("{}: started".format(miner))
                    self._started_miners.add(miner)
                    if self._idle_timeout:
                        log.debug("{} started; cancelling call to idle_callback_timeout()".format(miner))
                        GLib.source_remove(self._idle_timeout)
                        self._idle_timeout = None
                if miner in self._finished_miners:
                    self._finished_miners.remove(miner)
                    if self._idle_timeout:
                        log.debug("{} left Idle state; cancelling call to idle_callback_timeout()".format(miner))
                        GLib.source_remove(self._idle_timeout)
                        self._idle_timeout = None

    def _location_changed_callback(self, monitor, f1, f2, event_type):
        if event_type in [Gio.FileMonitorEvent.DELETED, Gio.FileMonitorEvent.UNMOUNTED]:
            log.debug("Location was deleted or unmounted; aborting.")
            self._complete()

    def _complete(self):
        log.debug("Removing watch on location")
        GObject.signal_handler_disconnect(self._monitor, self._location_watch)
        if self._idle_timeout:
            GLib.source_remove(self._idle_timeout)
            self._idle_timeout = None
        log.debug("Disconnecting signals")
        for miner, connection in self._signal_connections:
            GObject.signal_handler_disconnect(miner.miner_iface(), connection)
        log.debug("Calling idle_callback()")
        self.idle_callback()


# tracker_session_domain(): Access a session-wide Tracker domain.
#
# Args:
#   domain_name (str): name of the domain, or None to use the default. Note
#                      that custom session-wide domains must be installed into
#                      /usr, unlike when tracker_app_domain() is used.
#   timeout (int): seconds to wait for success before raising an error
#
@contextlib.contextmanager
def tracker_session_domain(domain_name=None, timeout=10):
    loop = MainLoop()

    domain = TrackerSessionDomain(domain_name)

    def start_callback():
        loop.quit()

    def timeout_callback():
        loop.quit()
        raise TrackerAppDomainError("Timeout after {} seconds waiting for domain.".format(timeout))

    domain.start_async(callback=start_callback)
    timeout_id = GLib.timeout_add_seconds(timeout, timeout_callback)
    loop.run_checked()
    GLib.source_remove(timeout_id)

    try:
        yield domain
    finally:
        domain.stop()


# tracker_app_domain(): Create an ephemeral Tracker domain
#
# Args:
#   domain_name (str): name of the domain, for example com.example.myproject
#   datadir (str): location to store all data files.
#   timeout (int): seconds to wait for success before raising an error
#   debug_verbosity (int): values between 1 and 3 enable logging of Tracker
#     and D-Bus status messages using the `logging` module.
#
@contextlib.contextmanager
def tracker_app_domain(domain_name, datadir, timeout=10, debug_verbosity=0):
    loop = MainLoop()

    domain = TrackerAppDomain(domain_name, datadir, debug_verbosity=debug_verbosity)

    def start_callback():
        loop.quit()

    def timeout_callback():
        loop.quit()
        raise TrackerAppDomainError("Timeout after {} seconds waiting for app domain.".format(timeout))

    domain.start_async(callback=start_callback)
    timeout_id = GLib.timeout_add_seconds(timeout, timeout_callback)
    loop.run_checked()
    GLib.source_remove(timeout_id)

    try:
        yield domain
    finally:
        domain.stop()
