Debugging Tracker subprocess crashes
------------------------------------

If you see failures from the test suite like this:

   GLib.Error: g-dbus-error-quark: GDBus.Error:org.freedesktop.DBus.Error.Spawn.ChildSignaled: Process com.example.Test.Tracker1.Miner.Files received signal 6 (26)

The actual problem is that a Tracker subprocess crashed, and so we can no longer
communicate with it over D-Bus.

To debug this issue it's necessary to look at the logs from the Tracker daemon.
Set the environment variable `TRACKER_APP_DOMAIN_TESTS_VERBOSITY=3` and run the
test suite again.

You may need to attach a debugger to the tracker process in question. The
recommended way to do this is to modify the code of the tracker process that
you want to debug and add the following two lines at the top of the main()
function:

   g_message("Waiting 10 seconds for debugger.");
   g_usleep(10 * G_USEC_PER_SEC);

Then run the tests, and use `gdb attach` to connect to the process.
