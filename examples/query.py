#!/usr/bin/python3
# Tracker App Domain
# Copyright (C) 2018  Sam Thursfield <sam@afuera.me.uk>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this program; if not, see <http://www.gnu.org/licenses/>.


'''Tracker app-domain query

This runs a query against metadata stored in an app-domain data directory. You
need to create this directory with one of the other example tools by passing
the --cachedir commandline argument.

'''


import trackerappdomain

from gi.repository import GLib

import argparse
import json
import logging
import os
import shutil
import sys
import tempfile

import utils.cliactions


def argument_parser():
    parser = argparse.ArgumentParser(description="Tracker app-domain inspector")
    parser.add_argument('--cachedir', help="Location of index", required=True, action=utils.cliactions.ExpandPath)
    parser.add_argument('--debug', help="Enable debug output", action='store_true')
    parser.add_argument('query', help="SPARQL query to run")
    return parser


def query(tracker, sparql):
    db = tracker.connection()

    results = []
    cursor = db.query(sparql)

    while cursor.next():
        row = {}
        for column in range(0,cursor.get_n_columns()):
            key = cursor.get_variable_name(column)
            value = cursor.get_string(column)[0]
            row[key] = value
        results.append(row)

    return results


def main():
    args = argument_parser().parse_args()

    if args.debug:
        logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

    if not os.path.exists(args.cachedir):
        raise RuntimeError("Directory {} does not exist.".format(args.cachedir))

    logging.info("Querying {}".format(args.cachedir))

    domain_name = 'org.gnome.sthursfield.trackerappdomain.examples'
    with trackerappdomain.tracker_app_domain(domain_name, args.cachedir) as t:
        results = query(t, args.query)
        json.dump(results, sys.stdout, indent=4)


try:
    main()
except RuntimeError as e:
    sys.stderr.write("{}\n".format(e))
    sys.exit(1)
