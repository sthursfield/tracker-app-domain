#!/usr/bin/python3
# Tracker App Domain
# Copyright (C) 2018  Sam Thursfield <sam@afuera.me.uk>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this program; if not, see <http://www.gnu.org/licenses/>.


'''Tracker app-domain media indexer

This is a very simple media indexer application which runs independently of any
session-wide Tracker instance, but uses the same Tracker code to do the search.

'''


import trackerappdomain

from gi.repository import GLib

import argparse
import json
import logging
import shutil
import sys
import tempfile

import utils.cliactions


def argument_parser():
    parser = argparse.ArgumentParser(description="Tracker app-domain media indexer example")
    parser.add_argument('dir', help="List media files from this directory")
    parser.add_argument('--cachedir', help="Location to store index", action=utils.cliactions.ExpandPath)
    parser.add_argument('--debug', help="Enable debug output", action='store_true')
    parser.add_argument('--json', help="Output results as JSON", action='store_true')
    return parser


def index_media(tracker, path):
    loop = trackerappdomain.MainLoop()

    result = []

    def progress_callback(status, progress, remaining_time):
        logging.debug("Waiting for indexing: {}, {}, {}".format(status, progress, remaining_time))

    def idle_callback():
        # Called when the miner becomes idle, which indicates everything we
        # enqueued has now been indexed.
        loop.quit()

    tracker.index_location_async(path, progress_callback, idle_callback)

    loop.run_checked()

    return result


def list_media(tracker):
    cursor = tracker.connection().query(' '.join([
        'SELECT ?url ?mimetype ?title ?music_artist ?music_album ?visual_width ?visual_height {',
            '?r a nfo:Media .',
            '?r nie:url ?url .',
            '?r nie:mimeType ?mimetype .',
            'OPTIONAL { ?r nie:title ?title . } ',
            'OPTIONAL { ?r nmm:performer [ nmm:artistName ?music_artist ] . }'
            'OPTIONAL { ?r nmm:musicAlbum [ nie:title ?music_album ] . }'
            'OPTIONAL { ?r nfo:width ?visual_width . }'
            'OPTIONAL { ?r nfo:height ?visual_height . }'
        '}',
        'ORDER BY COALESCE(?title, ?url)']))

    result = []

    while cursor.next():
        item = {
            'url': cursor.get_string(0)[0],
            'mimetype': cursor.get_string(1)[0],
            'title': cursor.get_string(2)[0],
            'music_artist': cursor.get_string(3)[0],
            'music_album': cursor.get_string(4)[0],
            'visual_width': cursor.get_string(5)[0],
            'visual_height': cursor.get_string(6)[0],
        }
        result.append(item)

    return result


def main():
    args = argument_parser().parse_args()

    if args.debug:
        logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

    logging.info("Indexing all media files in directory {}".format(args.dir))

    datadir = args.cachedir or tempfile.mkdtemp()
    try:
        domain_name = 'org.gnome.sthursfield.trackerappdomain.examples.MediaIndexer'
        with trackerappdomain.tracker_app_domain(domain_name, datadir) as t:
            t.clear_indexing_locations()

            index_media(t, args.dir)

            result = list_media(t)

            if args.json:
                def remove_nulls(item):
                    return {k:v for k, v in item.items() if v}
                json.dump([remove_nulls(item) for item in result], sys.stdout, indent=4)
            else:
                for item in result:
                    print("  * {}: {}".format(item['mimetype'], item['url']))
                    if item['title']:
                        print("    Title: {}".format(item['title']))
                    if item['music_artist']:
                        print("    Artist: {}".format(item['music_artist']))
                    if item['music_album']:
                        print("    Album: {}".format(item['music_album']))
                    if item['visual_width'] or item['visual_height']:
                        print("    Size: {} x {}".format(item['visual_width'], item['visual_height']))
    finally:
        if not args.cachedir:
            shutil.rmtree(datadir)


try:
    main()
except RuntimeError as e:
    sys.stderr.write("{}\n".format(e))
    sys.exit(1)
