#!/usr/bin/python3
# Tracker App Domain
# Copyright (C) 2018  Sam Thursfield <sam@afuera.me.uk>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this program; if not, see <http://www.gnu.org/licenses/>.


'''Tracker app-domain dump

This dumps all metadata stored in an app-domain data directory. You need to
create this directory with one of the other example tools by passing the
--cachedir commandline argument.

'''


import trackerappdomain

from gi.repository import GLib

import argparse
import json
import logging
import os
import shutil
import sys
import tempfile

import utils.cliactions


def argument_parser():
    parser = argparse.ArgumentParser(description="Tracker app-domain inspector")
    parser.add_argument('--cachedir', help="Location of index", required=True, action=utils.cliactions.ExpandPath)
    parser.add_argument('--debug', help="Enable debug output", action='store_true')
    return parser


def dump(tracker):
    db = tracker.connection()

    results = {}
    cursor1 = db.query('SELECT ?r { ?r a nie:InformationElement . }')

    while cursor1.next():
        uri = cursor1.get_string(0)[0]

        resource = {}
        cursor2 = db.query('SELECT ?p ?v { <%s> ?p ?v }' % uri)
        while cursor2.next():
            prop = cursor2.get_string(0)[0]
            value = cursor2.get_string(1)[0]
            resource[prop] = value
        results[uri] = resource

    return results


def main():
    args = argument_parser().parse_args()

    if args.debug:
        logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

    if not os.path.exists(args.cachedir):
        raise RuntimeError("Directory {} does not exist.".format(args.cachedir))

    logging.info("Dumping data from {}".format(args.cachedir))

    domain_name = 'org.gnome.sthursfield.trackerappdomain.examples.MediaIndexer'
    with trackerappdomain.tracker_app_domain(domain_name, args.cachedir) as t:
        json.dump(dump(t), sys.stdout, indent=4)


try:
    main()
except RuntimeError as e:
    sys.stderr.write("{}\n".format(e))
    sys.exit(1)
