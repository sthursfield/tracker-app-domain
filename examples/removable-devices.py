#!/usr/bin/python3
# Tracker App Domain
# Copyright (C) 2018  Sam Thursfield <sam@afuera.me.uk>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this program; if not, see <http://www.gnu.org/licenses/>.


'''Tracker app-domain removable device indexer

Demonstrates best practice for dealing with removable devices with Tracker.

'''


import trackerappdomain

from gi.repository import Gio, GLib

import argparse
import json
import logging
import os
import shutil
import sys
import tempfile

import utils.cliactions


def argument_parser():
    parser = argparse.ArgumentParser(description="Tracker app-domain removable device indexer")
    parser.add_argument('--cachedir', help="Location of index", required=True, action=utils.cliactions.ExpandPath)
    parser.add_argument('--debug', help="Enable debug output", action='store_true')
    parser.add_argument('--mount-path', help="Path to the device's mountpoint")
    return parser


def show_volumes():
    volume_monitor = Gio.VolumeMonitor.get()

    drives = volume_monitor.get_connected_drives()
    volumes = volume_monitor.get_volumes()
    mounts = volume_monitor.get_mounts()

    print("Drives: {}".format([drive.get_name() for drive in drives]))
    print("Volumes: {}".format([volume.get_name() for volume in volumes]))
    print("Mounts: {}".format([
        {'name': mount.get_name(), 'root': mount.get_root().get_path()} for mount in mounts]))


def main():
    args = argument_parser().parse_args()

    if args.debug:
        logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

    if args.mount_path:
        domain_name = 'org.gnome.sthursfield.trackerappdomain.examples'
        with trackerappdomain.tracker_app_domain(domain_name, args.cachedir, debug_verbosity=2) as t:
            t.clear_indexing_locations()

            loop = trackerappdomain.MainLoop()
            def progress_callback(status, progress, remaining):
                print("Status: {}; {}% complete, {} seconds remaining.".format(status, progress * 100, remaining))
            def complete_callback():
                loop.quit()

            t.index_location_async(args.mount_path, progress_callback, complete_callback)

            loop.run_checked()

            print("Indexing {} complete.".format(args.mount_path))
    else:
        show_volumes()


try:
    main()
except RuntimeError as e:
    sys.stderr.write("{}\n".format(e))
    sys.exit(1)
