#!/usr/bin/python3
# Tracker App Domain
# Copyright (C) 2018  Sam Thursfield <sam@afuera.me.uk>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this program; if not, see <http://www.gnu.org/licenses/>.


'''Tracker app-domain search

This is a very simple search application which runs independently of any
session-wide Tracker instance, but uses the same Tracker code to do the search.

'''


import trackerappdomain

from gi.repository import GLib

import argparse
import logging
import shutil
import sys
import tempfile


def argument_parser():
    parser = argparse.ArgumentParser(description="Tracker app-domain search example")
    parser.add_argument('dir', help="Directory to search through")
    parser.add_argument('text', help="Search text")
    parser.add_argument('--debug', help="Enable debug output", action='store_true')
    return parser


def search(tracker, path, text):
    loop = trackerappdomain.MainLoop()

    result = []

    def progress_callback(status, progress, remaining_time):
        logging.debug("Waiting for indexing: {}, {}, {}".format(status, progress, remaining_time))

    def idle_callback():
        logging.debug("Indexing complete, doing search")
        db = tracker.connection()
        cursor = db.query(
            "SELECT tracker:coalesce (nie:url(?s), ?s) fts:snippet(?document) "
            "    WHERE {"
            "        ?s fts:match \"%s\" ."
            "    } "
            "    ORDER BY nie:url(?s) " % text)
        while cursor.next():
            result.append(cursor.get_string(0)[0])
        loop.quit()

    tracker.index_location_async(path, progress_callback, idle_callback)

    loop.run_checked()

    return result


def main():
    args = argument_parser().parse_args()

    if args.debug:
        logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

    logging.info("Searching for {} in directory {}".format(args.text, args.dir))

    base = tempfile.mkdtemp()
    try:
        with trackerappdomain.tracker_app_domain('org.gnome.sthursfield.trackerappdomain.examples.search', base) as t:
            t.clear_indexing_locations()
            result = search(t, args.dir, args.text)
            if len(result) > 0:
                print("Results:\n  {}".format('\n  '.join(result)))
            else:
                print("No results.")
    finally:
        shutil.rmtree(base)


try:
    main()
except RuntimeError as e:
    sys.stderr.write("{}\n".format(e))
    sys.exit(1)
