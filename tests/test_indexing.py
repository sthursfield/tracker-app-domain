# Tracker App Domain
# Copyright (C) 2018  Sam Thursfield <sam@afuera.me.uk>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this program; if not, see <http://www.gnu.org/licenses/>.


import gi
gi.require_version('GdkPixbuf', '2.0')
from gi.repository import GdkPixbuf, GLib, GObject

import pytest

import os

import trackerappdomain

# This is for digging into internals.
import trackerappdomain.trackerappdomain


_verbosity = os.environ.get('TRACKER_APP_DOMAIN_TESTS_VERBOSITY')
if _verbosity:
    import logging, sys
    logging.basicConfig(level=logging.DEBUG, stream=sys.stdout)

    TRACKER_VERBOSITY = int(_verbosity)
else:
    TRACKER_VERBOSITY = 0


@pytest.fixture
def content(tmpdir):
    '''Fixture which provides a directory containing some media content.'''
    def generate_jpeg(filename):
        pixbuf = GdkPixbuf.Pixbuf.new(colorspace=GdkPixbuf.Colorspace.RGB,
                                    has_alpha=False, bits_per_sample=8, width=10,
                                    height=10)
        pixbuf.savev(filename, 'jpeg', [], [])

    content_dir = tmpdir.join('content').mkdir()
    for i in range(0, 10):
        generate_jpeg(str(content_dir.join('image{}.jpg'.format(i))))

    return {
        'path': str(content_dir),
        'n_images': 10
    }


def n_images(domain):
    cursor = domain.connection().query('SELECT COUNT(?url) { ?url a nfo:Image, nie:DataObject }')
    cursor.next()
    return cursor.get_integer(0)


def test_app_domain_indexing(tmpdir, content):
    '''Test that we can indexing a location on request.'''

    domain_dir = tmpdir.join('domain').mkdir()
    with trackerappdomain.tracker_app_domain('com.example.Test', domain_dir, debug_verbosity=TRACKER_VERBOSITY) as t:
        t.clear_indexing_locations()

        loop = trackerappdomain.MainLoop()
        def progress_callback(status, progress, remaining):
            print("Status: {}; {}% complete, {} seconds remaining.".format(status, progress * 100, remaining))

        def complete_callback():
            loop.quit()

        t.index_location_async(content['path'], progress_callback, complete_callback)

        loop.run_checked()

        assert n_images(t) == content['n_images']
