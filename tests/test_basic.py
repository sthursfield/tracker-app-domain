# Tracker App Domain
# Copyright (C) 2018  Sam Thursfield <sam@afuera.me.uk>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License as published by the Free Software Foundation; either
# version 2 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU Library General Public
# License along with this program; if not, see <http://www.gnu.org/licenses/>.


import pytest

import trackerappdomain

# This is for digging into internals.
import trackerappdomain.trackerappdomain

import sys


# Uncomment this for debug logs!
#import logging, sys
#logging.basicConfig(level=logging.DEBUG, stream=sys.stdout)

# Set to 3 for more debug logs!
TRACKER_VERBOSITY = 0


def test_session_domain_basic():
    '''Simple test that we can contact the session-wide Tracker domain.'''
    with trackerappdomain.tracker_session_domain() as t:
        db = t.connection()
        # Check some self-referential axiom of the RDF store.
        assert db.query('ASK { rdfs:Resource rdf:type rdfs:Resource }')


def test_app_domain_basic(tmpdir):
    '''Simple test that we can set up a private Tracker domain.'''
    with trackerappdomain.tracker_app_domain('com.example.Test', tmpdir, debug_verbosity=TRACKER_VERBOSITY) as t:
        db = t.connection()

        # Check some self-referential axiom of the RDF store.
        assert db.query('ASK { rdfs:Resource rdf:type rdfs:Resource }')

    # Test that we can do the same thing again. Sounds dumb but this can break!
    with trackerappdomain.tracker_app_domain('com.example.Test', tmpdir, debug_verbosity=TRACKER_VERBOSITY) as t:
        db = t.connection()
        assert db.query('ASK { rdfs:Resource rdf:type rdfs:Resource }')


def test_session_domain_read_configuration():
    '''Read configuration from the session-wide domain.'''
    with trackerappdomain.tracker_session_domain() as t:
        # We can't make any assertions about the session domain's configuration
        # on a given machine. We just test that nothing crashes when we query it.
        print(t.configured_indexing_locations)


def test_app_domain_read_configuration(tmpdir):
    '''Read configuration from a private domain.'''
    with trackerappdomain.tracker_app_domain('com.example.Test', tmpdir, debug_verbosity=TRACKER_VERBOSITY) as t:
        # The com.example.Test profile is a real DConf database like any other,
        # so it may have existing settings that we need to empty out.
        t.reset_config()

        # The config should equal Tracker's default values.
        default_locations = [
            ('&DESKTOP', True),
            ('&DOCUMENTS', True),
            ('&DOWNLOAD', True),
            ('&MUSIC', True),
            ('&PICTURES', True),
            ('&VIDEOS', True),
            ('$HOME', False),
        ]
        assert t.configured_indexing_locations == [
            (trackerappdomain.trackerappdomain.expand_xdg_placeholders(path), recursive)
            for path, recursive in default_locations]


def test_session_domain_status():
    with trackerappdomain.tracker_session_domain() as t:
        # We can't know what state the session domain will be in, we just check
        # that we can read these properties without anything crashing.
        print(t.indexing)
        print(t.status_message)


def test_app_domain_status(tmpdir):
    with trackerappdomain.tracker_app_domain('com.example.Test', tmpdir, debug_verbosity=TRACKER_VERBOSITY) as t:
        assert t.indexing == False
        assert t.status_message == 'Idle'
