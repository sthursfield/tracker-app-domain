Tracker App Domain helper
=========================

> Eventually this module should become obsolete and Tracker itself should provide
the necessary API to allow it to be used as a library from applications.

This has now happened. Tracker 3 is the future and this project is obsolete,
as of September 2020.
